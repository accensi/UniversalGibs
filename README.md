### Installation
---
Download the zip and load it into GZDoom. No need to change its extension to .pk3.

### Important
---
- Requires at least GZDoom 4.5.0!

### Why would this interest you?
---
- It's a less resource-intensive than Ketchup/Bolognese. Mostly because it doesn't spam actors and models. Still, it may lag. Caution against overuse is advised.
- Crushers don't halt the map to a freeze.
- If you like the gibbing in Blood 2 or Shogo, this definitely comes close to those.
- It's highly customizable. Including but not limited to gib modes like Mortal Kombat bones, flowers, Half-Life gibs, and more!
- It uses particles, so it's entirely possible to have that blocky blood.
- But wait, there's more! It can also use vanilla blood, making it fully compatible with mods such as Droplets, and even Nashgore or Bolognese!
- It's interfaceable from addons, allowing you to override various functions by inheriting from the main handler and calling DisableMainHandler() in a WorldLoaded override.
- It's universal.

### F.A.Q.
---
Q: *Is this compatible with other IWADs/PWADs?*  
A: Yes.

Q: *Is this multiplayer compatible?*  
A: Yes.

Q: *IS DIS COMPATIBIEL WIHT BRUTAL DOOM!?!*  
A: Funnily enough, yes. It actually is.

Q: *Does this feature new blood?*  
A: Yes, but it's optional.

### (In)Compatibility & Known Issues
---
Certain mods may behave weirdly with UG:
- **Hideous Destructor:** Most weapons have +NOEXTREMEDEATH on the puffs/projectiles, so if you want this to work with HD, flip the respective override in Advanced Settings. Another issue is that Advanced Physics are messed up and won't work right sometimes. Check out the Compatibility Options menu.
 
Other issues:
- Monsters that don't end their death state with a -1 frame duration won't be able to have their corpses gibbed.
- Smarter and Always sprite clipping settings in Display Options -> Hardware Renderer will cause gibs to look kinda wack when dissolving. It's just a visual bug.

### Blacklisting Classes
---
- You don't need to touch UG to do that. If you have classes in your own mod that you wish to be blacklisted, add an UGBLKLST lump to your mod's root directory, then separate each blacklisted class with a comma. You can place them on new lines and such, but don't use comments.

Example: `LostSoul, Revenant, SpiderMastermind, Cyberdemon, BossBrain`